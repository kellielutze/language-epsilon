# Contributing

Contributions are welcome and encouraged, this project has been created in my free time and is available for all to use and modify as they please.

Contributions to the project setup as well as functionality are welcome. Feature requests and defects, while welcome are not guarateed to be implemented or resolved in any fixed timeframe.


## Environment Setup

This project is built using the following:

- [Nodejs 17](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/getting-started/install)

## Running

1. Install dependencies. Run `yarn` in the project root.
2.

## Testing

Tests are available using `.spec` files.

### Running tests

1. From the project root run `yarn test`

## Reporting defects

Please log defects using [GitLab issues](https://gitlab.com/kellielutze/language-epsilon/-/issues/new?issuable_template=Defect). A defect issue template has been provided to get you started.

## Feature requests.

Please log feature requests using [GitLab issues](https://gitlab.com/kellielutze/language-epsilon/-/issues/new?issuable_template=Task). A `Task` or `Story` issue template has been provided to get you started.

## Merge requests

Merge requests are welcome and encouraged, an issue is only required for large changes. Small changes with an adequate MR description are accepted.

Please set all MR's to merge into the `main` branch.

## Publising

This extension is published as documented by Microsoft in their [Publishing Extensions](https://code.visualstudio.com/api/working-with-extensions/publishing-extension) page.

1. Install the command line tool `yarn global add vsce`
2. Package `vsce package`
3. Publish `vsce publish`

## Versioning

This repository is versioned using [semantic versioning](https://semver.org/). Each new release will be recorded in the [CHANGELOG](CHANGELOG.md) with the repo being tagged with the relevant version number.

## Relevant documentation

- [VS Code Syntax Highlight Guide](https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide)

