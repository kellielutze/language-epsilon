# Language epsilon

Syntax highlighting and grammars for the [Eclipse Epsilon](https://www.eclipse.org/epsilon/) set of languages for VS Code.

## Installation

There are two key ways to install this extension.

### Marketplace

The extension can be installed from the following marketplaces.

- [Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=kellielutze.language-epsilon)

### Using Git

TODO

## Usage

### Configuration

The following config options are available and can be set in your VS Code config.

```json
// The number of diagnostic errors to show
"languageServerEpsilon.maxNumberOfProblems": 1000,

"languageServerEpsilon.trace.server": "verbose",

// Show diagnostic errors for duplicate protected regions in a single file
"languageServerEpsilon.lintDuplicateProtectedRegionIds": true,

// Show diagnostic errors for the opininated style
"languageServerEpsilon.lintStyle": true,

// Set the debug log level, options are: debug, info, error, none
"languageServerEpsilon.logLevel": "error",
```
## Features

- Syntax highlighting for EOL, EGX and EGL
- Snippets for EOL, EGX and EGL

## Build

TODO

## Related Projects

- [epsilon-language-tools](https://gitlab.com/kellielutze/epsilon-language-tools) - Experimental tooling enhancements implemented using a from scratch basic language server.
- [Epsilon Package for Sublime](https://github.com/epsilonlabs/sublime) - Package that extends the Sublime text editor with support for syntax-highlighting Epsilon programs, Flexmi models and Emfatic metamodels.

## Contribute

Contributions are welcome welcome. Please see [Contributing](CONTRIBUTING.md) for details.


## License

[MIT](./LICENSE)