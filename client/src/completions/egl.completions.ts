import { CompletionContext, CompletionItem, CompletionItemKind, languages, Position, SnippetString, TextDocument } from 'vscode';
import { CancellationToken } from 'vscode-jsonrpc';

export const EglCompletions: Array<CompletionItem>= [

	{
		label: 'static section',
		kind: CompletionItemKind.Snippet,
		insertText: new SnippetString('[%= ${1} %]'),
		detail: '[%= <section contents> %]',
		documentation: "Start of a static section."
	},
	{
		label: 'dynamic section',
		kind: CompletionItemKind.Snippet,
		insertText: new SnippetString('[% ${1} %]'),
		detail: '[%= <section contents> %]',
		documentation: "Start of a static section."
	}
];

export const eglCompletionProvider = languages.registerCompletionItemProvider('language-egl', {
	provideCompletionItems(document: TextDocument, position: Position,
		token: CancellationToken, context: CompletionContext) {
		return EglCompletions;
	}
});