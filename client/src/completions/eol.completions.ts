import { CompletionContext, CompletionItem, CompletionItemKind, languages, Position, SnippetString, TextDocument } from "vscode";
import { CancellationToken } from "vscode-jsonrpc";

export const EolCompletions: Array<CompletionItem> = [
					{
						label: ".isTypeOf()",
						kind: CompletionItemKind.Function,
						insertText: new SnippetString("isTypeOf(${1:element})"),
						detail: "isTypeOf(type: Type): Boolean",
						documentation:
							"Returns true if the object is of the given type and false otherwise"
					},
					{
						label: ".isKindOf()",
						kind: CompletionItemKind.Function,
						insertText: new SnippetString("isTypeOf(${1:element})"),
						detail: "<object>.isTypeOf(type: Type): Boolean",
						documentation:
							"Returns true if the object is of the given type or one of its subtypes and false otherwise"
					},
					{
						label: "import ",
						kind: CompletionItemKind.Keyword,
						insertText: new SnippetString("import '$1';"),
						detail: "import <module.eol>;",
						documentation:
							"An ETL module can import a number of other ETL modules"
					},
					{
						kind: CompletionItemKind.Function,
						label: ".err() ",
						insertText: new SnippetString(
							"err('{1:[prefix : String]}');"
						),
						detail: "<object>.err([prefix : String]) : Any",
						documentation:
							"Prints a string representation of the object on which it is invoked to the error stream prefixed with the optional prefix string and returns the object on which it was invoked. In this way, the err operation can be used for debugging purposes in a non-invasive manner"
					},
					{
						label: ".format()",
						kind: CompletionItemKind.Function,
						insertText: new SnippetString(
							"format('${1:[pattern : String]}');"
						),
						detail: "format([pattern : String]) : String",
						documentation:
							"Uses the provided pattern to form a String representation of the object on which the method is invoked. The pattern argument must conform to the format string syntax defined by Java"
					},
					{
						kind: CompletionItemKind.Function,
						label: ".println()",
						detail: "<object>.println([prefix: String]): Any",
						insertText: new SnippetString(
							"println('${1:[prefix: String]}');"
						),
						documentation:
							"Has the same effects as the print operation but also produces a new line in the output stream."
					},
					{
						label: ".print()",
						kind: CompletionItemKind.Function,
						detail: "<object>.print([prefix: String]): Any",
						insertText: new SnippetString(
							"print('${1:[prefix: String]}');"
						),
						documentation:
							"Prints  a  string  representation  of  the  object  on which it is invoked to the regular output stream,prefixed  with  the  optional prefix string  and  re-turns the object on which it was invoked.  In this way,  the print operation can be used for debugging purposes in a non-invasive manner"
					},
					{
						label: ".ifUndefined()",
						kind: CompletionItemKind.Function,
						insertText: new SnippetString("ifUnderfined($1);"),
						detail: "<object>.ifUndefined(alt: Any): Any",
						documentation:
							"If the object is undefined, it returns alt else it returns the object"
					},
					{
						label: ".hasProperty()",
						kind: CompletionItemKind.Function,
						insertText: new SnippetString("hasProperty('${1:name}');"),
						detail: "hasProperty(name: String): Boolean",
						documentation:
							"Returns true if the object has a property with the specified name or false otherwise"
					},
					{
						label: ".errln()",
						kind: CompletionItemKind.Function,
						detail: "errln([prefix : String]) : Any",
						insertText: new SnippetString("errln('${1:[prefix : String]}');"),
						documentation:
							"Has the same effects as the err operation but also produces a new line in the output stream."
					}
				];

export const eolCompletionProvider = languages.registerCompletionItemProvider(
	"language-eol",
	{
		provideCompletionItems(
			document: TextDocument,
			position: Position,
			token: CancellationToken,
			context: CompletionContext
		) {
			return EolCompletions;
		}
	}
);
