/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import * as path from 'path';
import { ExtensionContext, languages, workspace } from 'vscode';

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	TransportKind
} from 'vscode-languageclient/node';
import { FixWhiteSpace } from './code-actions/egl.code-actions';
import { EglFormatter } from './code-actions/format-provider';
import { eglCompletionProvider } from './completions/egl.completions';
import { eolCompletionProvider } from './completions/eol.completions';

let client: LanguageClient;

export function activate(context: ExtensionContext) {
	// The server is implemented in node
	const serverModule = context.asAbsolutePath(
		path.join('server', 'out', 'server.js')
	);
	// The debug options for the server
	// --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to the server for debugging
	const debugOptions = { execArgv: ['--nolazy', '--inspect=6009'] };

	// If the extension is launched in debug mode then the debug server options are used
	// Otherwise the run options are used
	const serverOptions: ServerOptions = {
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: {
			module: serverModule,
			transport: TransportKind.ipc,
			options: debugOptions
		}
	};

	// Options to control the language client
	let clientOptions: LanguageClientOptions = {
		// Register the server for epsilon documents
		documentSelector: [
			{ scheme: 'file', language: 'language-egl' },
			{ scheme: 'file', language: 'language-eol' },
			{ scheme: 'file', language: 'language-egx' }
		],
		synchronize: {
			// Notify the server about file changes to '.clientrc files contained in the workspace
			fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
		}
	};

	// Create the language client and start the client.
	client = new LanguageClient(
		'languageServerEpsilon',
		'Language Server Epsilon',
		serverOptions,
		clientOptions
	);

	const languageEgl = "language-egl";


	// Activate any providers
	context.subscriptions.push(
		// Completion
		eglCompletionProvider,
		eolCompletionProvider,

		// Code Actions
		languages.registerCodeActionsProvider(languageEgl, new FixWhiteSpace(), {
			providedCodeActionKinds: FixWhiteSpace.providedCodeActionKinds
		}),

		// Formatter
		languages.registerDocumentFormattingEditProvider(languageEgl, new EglFormatter())
	);

	// Start the client. This will also launch the server
	client.start();
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}

