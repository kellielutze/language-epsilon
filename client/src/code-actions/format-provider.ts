import { CancellationToken, DocumentFormattingEditProvider, FormattingOptions, Position, ProviderResult, Range, TextDocument, TextEdit } from "vscode";

export class EglFormatter implements DocumentFormattingEditProvider {
	provideDocumentFormattingEdits(
		document: TextDocument,
		options: FormattingOptions,
		token: CancellationToken
	): ProviderResult<TextEdit[]> {
		return [...this.formatStaticSection(document)];
	}

	private formatStaticSection(document: TextDocument): TextEdit[] {
		const content = document.getText();
		const contents = content.split("\n");

		let textEdits: TextEdit[] = [];

		const lineFormatters = [this.formatStaticSectionStart, this.formatStaticSectionEnd];

		contents.forEach((line, index) => {
			lineFormatters.forEach(formatter => {
				// Split as we may have multiple matches
				let matches = line.match(/\[%=(.*?)%\]/gm);
				if (matches) {
					matches.forEach(item => {
						let result = formatter(item, index, line.indexOf(item));
						if (result) {
							textEdits = [...textEdits, ...result];
						}
					});
				}
			});
		});

		return textEdits;
	}

	private formatStaticSectionEnd(line: string, index: number, characterStart: number): TextEdit[] {
		const edits: TextEdit[] = [];

		let match = line.match(/(\s*)?%\]/);
		if (match) {
			let matched = match[0];
			let character = characterStart + line.indexOf(matched);

			edits.push(
				new TextEdit(new Range(new Position(index, character), new Position(index, character + matched.length)), " %]")
			);
		}

		return edits;
	}

	private formatStaticSectionStart(line: string, index: number, characterStart: number): TextEdit[] {
		const edits: TextEdit[] = [];

		let match = line.match(/\[%=(\s+)?/);
		if (match) {
			let matched = match[0];
			let character = characterStart + line.indexOf(matched);

			edits.push(
				new TextEdit(new Range(new Position(index, character), new Position(index, character + matched.length)), "[%= ")
			);
		}

		return edits;
	}
}
