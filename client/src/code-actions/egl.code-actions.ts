import { CancellationToken, CodeAction, CodeActionContext, CodeActionKind, CodeActionProvider, Diagnostic, Position, Range, Selection, TextDocument, WorkspaceEdit } from "vscode";

const COMMAND = "code-actions-quickfix.fix-white-space";

type StaticSectionActionCode = 'static-section-start-space' | 'static-section-end-space';
type Fixes = {
	[key in StaticSectionActionCode]: (document: TextDocument, range: Range, diagnostic: Diagnostic) => CodeAction
};
const MissingWhiteSpaceCommandLabel = "Add missing white space";

export class FixWhiteSpace implements CodeActionProvider {
	public static readonly providedCodeActionKinds = [CodeActionKind.QuickFix];

	provideCodeActions(
		document: TextDocument,
		range: Range | Selection,
		context: CodeActionContext,
		token: CancellationToken
	): CodeAction[] {
		const fixes: Fixes= {
			'static-section-start-space': this.addMissingWhiteSpace,
			'static-section-end-space': this.addMissingWhiteSpace
		};

		return context.diagnostics
			.filter(diagnostic => fixes[diagnostic.code as string])
			.map(diagnostic => fixes[diagnostic.code as string](document, range, diagnostic));
	}

	private addMissingWhiteSpace(document: TextDocument, range: Range, diagnostic: Diagnostic): CodeAction {
		const action = new CodeAction(MissingWhiteSpaceCommandLabel, CodeActionKind.QuickFix);
		action.command = {
			command: COMMAND,
			title: "Auto fix white space at the end of the static section",
			tooltip: "This will attempt to auto fix the white space at the end of the static section."
		};
		action.edit = new WorkspaceEdit();
		const sectionRange = document.getWordRangeAtPosition(
			new Position(range.start.line, range.start.character),
			/\[%=(.*?)%\]/gm
		);

		let content = document.getText(sectionRange);

		// Magic the string
		if (content[3] != ' ' && content[3] != '	') {
			content = content.replace('[%=', '[%= ');
		}

		const expectedSpace = content[content.length - 3];
		if (expectedSpace != " " && expectedSpace != '	') {
			content = content.replace('%]', ' %]');
		}

		action.edit.replace(document.uri, sectionRange, content);
		action.diagnostics = [diagnostic];
		return action;
	}
}
