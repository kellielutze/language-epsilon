import { DiagnosticSeverity } from 'vscode-languageserver';
import { LintingError } from '../interfaces/lintingError';


export const ERRORS: Array<LintingError> = [
					{
						match: /^(\s?%\])/gm,
						message:
							"Template closing character should be at the end of the previous line",
						severity: DiagnosticSeverity.Warning
					},
					// {
					// 	match: /\[%=?.+?(?=\[%)/gm,
					// 	message:
					// 		"Unclosed section tag.",
					// 	severity: DiagnosticSeverity.Error
					// }
				];

export const DUPLICATE_PR: LintingError = {
	match: /\[%\= ?protected\(out, ["|'](.+)["|'], ["|']["|']\) ?%\]/gm,
	message: 'Duplicate protected region ID',
	severity: DiagnosticSeverity.Error,
};