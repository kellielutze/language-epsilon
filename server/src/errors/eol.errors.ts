import { DiagnosticSeverity } from 'vscode-languageserver';
import { LintingError } from '../interfaces/lintingError';

export const ERRORS: Array<LintingError> = [
	{
		match: /(".+")/gm,
		message: `Use single quotes`,
		severity: DiagnosticSeverity.Warning
	}
];