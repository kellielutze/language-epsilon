import { CompletionItem, CompletionItemKind } from 'vscode-languageserver-types';

export const EgxCompletions = [
	{
		label: 'rule ',
		kind: CompletionItemKind.Keyword,
		data: 1
	},
	{
		label: 'transform ',
		kind: CompletionItemKind.Keyword,
		data: 2
	},
	{
		label: 'guard ',
		kind: CompletionItemKind.Keyword,
		data: 3
	},
	{
		label: 'transform ',
		kind: CompletionItemKind.Keyword,
		data: 4
	}
];

export function ResolveEgxCompletion(item: CompletionItem) {
	if (item.data === 1) {
		item.detail = 'rule <name>';
		item.documentation =

		`Allows transformation of an arbitrary number of source models to the target model.
rules can also be extended by other rules (comma separated) using the extends keyword.`;

	} else if (item.data === 2) {
		item.detail = 'transform <sourceParameterName>:<sourceParameterType>';
		item.documentation = '';

	} else if (item.data === 3) {
		item.detail = 'guard (:expression)|({statementBlock}))?';
		item.documentation =

		`Use an expression that returns a boolean for your guard
e.g  guard: userEntityMap().containsKey(entity.Name).A guard is part of a rule.`;
	}
	return item;
}
