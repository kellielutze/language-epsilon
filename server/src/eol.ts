import { Connection, Diagnostic } from 'vscode-languageserver';
import { TextDocument } from 'vscode-languageserver-textdocument';
import { ERRORS } from './errors/eol.errors';
import { Source } from './interfaces/sources.enum';
import { getDocumentSettings } from './server';

export async function validateEOLDocument(textDocument: TextDocument, connection: Connection,
	hasDiagnosticRelatedInformationCapability: Boolean) {

	// In this simple example we get the settings for every validate run.
	let settings = await getDocumentSettings(textDocument.uri);

	// The validator creates diagnostics for all uppercase words length 2 and more
	let text = textDocument.getText();
	let m: RegExpExecArray | null;

	let problems = 0;
	let diagnostics: Diagnostic[] = [];

	ERRORS.forEach(error => {
		while ((m = error.match.exec(text)) && problems < settings.maxNumberOfProblems) {
			problems++;
			let diagnostic: Diagnostic = {
				severity: error.severity,
				range: {
					start: textDocument.positionAt(m.index),
					end: textDocument.positionAt(m.index + m[0].length)
				},
				message: error.message,
				source: Source.epsilonLanguageEol
			};
			diagnostics.push(diagnostic);
		}
	});

	// Send the computed diagnostics to VSCode.
	connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
}