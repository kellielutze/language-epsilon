import { Connection, Diagnostic, DiagnosticRelatedInformation, DiagnosticSeverity, Position } from "vscode-languageserver";
import { TextDocument } from 'vscode-languageserver-textdocument';
import { LintSettings, LogLevel } from "../interfaces/lintingError";
import { Source } from "../interfaces/sources.enum";
import { EOF_TOKEN, Token, TokenKeys } from "../interfaces/tokens";
import { EglAst, ProtectedRegion } from "./egl.ast";
import { TokenStream } from "./token-stream";


export enum StaticSectionActionCode {
	StartSpace = "static-section-start-space",
	EndSpace = "static-section-end-space"
}

export enum DynamicSectionActionCode {
	StartSpace = "dynamic-section-start-space",
	EndSpace = "dynamic-section-end-space"
}

export enum DiagnosticType {
	STYLE,
	SYNTAX
}

/**
 * To operate on a single static section
 */
export class EglStaticSectionParser {
	private _tokenStream: TokenStream;
	private _textDocument: TextDocument;
	private _errors: Array<Diagnostic>;
	private _connection: Connection;
	private _eglAst: EglAst;
	private _settings: LintSettings;

	constructor(
		tokenStream: TokenStream,
		textDocument: TextDocument,
		connection: Connection,
		eglAst: EglAst,
		settings: LintSettings
	) {
		this._tokenStream = tokenStream;
		this._textDocument = textDocument;
		this._connection = connection;
		this._eglAst = eglAst;
		this._settings = settings;
		this._errors = [];
	}

	parse(): Array<Diagnostic> {
		this._errors = [];

		this.log(JSON.stringify(this._tokenStream.getTokenNames()));

		const nonWhiteSpaceTokenCount = this._tokenStream.getTokenNames().filter(name => name != "SPACE").length;
		if (nonWhiteSpaceTokenCount < 1) {
			this.raiseError("Empty static section", DiagnosticType.SYNTAX);
			return this._errors;
		}

		if (this._tokenStream.checkToken(TokenKeys.SPACE, this._connection)) {
			this._tokenStream.consumeToken();
		} else {
			this.raiseWarning("Missing space between start of static section and section contents.", DiagnosticType.STYLE, StaticSectionActionCode.StartSpace);
		}

		this.parseContent();

		if (!this._tokenStream.checkToken(TokenKeys.SPACE, this._connection)) {
			this.raiseWarning(
				"Missing space between the static section contents and the end of the section.",
				DiagnosticType.STYLE, StaticSectionActionCode.EndSpace
			);
		} else {
			this._tokenStream.consumeToken();
		}

		return this._errors;
	}

	private parseContent() {
		this.traceStep("Parse content");
		while (this._tokenStream.nextToken() != EOF_TOKEN) {
			if (this._tokenStream.currentTokenKey() === TokenKeys.SPACE) {
				this._tokenStream.consumeToken();
				this.parseContent();
			} else {
				this.parseExpression();
			}
		}
	}

	private parseExpression() {
		this.logStep("Parse expression");
		if (this._tokenStream.checkTokens([TokenKeys.IDENTIFIER, TokenKeys.KW_PROTECTED])) {
			if (this._tokenStream.checkNextTwoTokens([TokenKeys.OP_PLUS])) {
				this.parseOperation();
			} else if (this._tokenStream.checkNextTokens([TokenKeys.L_BRACKET])) {
				this.parseMethod();
			} else {
				this.parseIdentifier();
			}
		} else {
			this.raiseError(
				`Unexpected token -> Expected <expression> but got ${this._tokenStream.currentToken().value}`,
				DiagnosticType.SYNTAX
			);
			this._tokenStream.consumeToken();
		}
	}

	private parseIdentifier() {
		this.traceStep("Parse identifier");
		if (this._tokenStream.checkNextTokens([TokenKeys.L_BRACKET])) {
			this.parseMethod();
		} else if (this._tokenStream.nextTokenKey() == TokenKeys.DOT) {
			this.parseAttributeAccess();
		} else {
			this._tokenStream.consumeToken();
		}
	}

	parseMethod() {
		this.logStep("Parse method");

		if (this._tokenStream.currentTokenKey() == TokenKeys.KW_PROTECTED) {
			this.parseProtected();
		} else {
			this.consumeSpecificToken(TokenKeys.IDENTIFIER);
			this.consumeSpecificToken(TokenKeys.L_BRACKET);

			if (this._tokenStream.currentTokenKey() === TokenKeys.IDENTIFIER) {
				this.parseExpression();
			}

			// Handle random whitespace
			this.consumeInvalidWhitespace(TokenKeys.R_BRACKET);

			this.consumeSpecificToken(
				TokenKeys.R_BRACKET,
				`Unexpected token -> Expected ) but got ${this._tokenStream.currentToken().value}`
			);
		}
	}

	private parseAttributeAccess() {
		this.traceStep("Parse attribute access");
		this.consumeSpecificToken(TokenKeys.IDENTIFIER);
		this.consumeSpecificToken(TokenKeys.DOT);

		// Identifier must follow dot
		if (!this._tokenStream.checkTokens([TokenKeys.IDENTIFIER])) {
			this.raiseError(`Expecting IDENTIFIER but found ${this._tokenStream.currentTokenKey()}`, DiagnosticType.SYNTAX);
		}
	}

	private parseProtected() {
		const startLine = this._tokenStream.currentToken().location.line;
		const start = this._tokenStream.currentToken().location.start;

		this.consumeSpecificToken(TokenKeys.KW_PROTECTED);
		this.consumeSpecificToken(TokenKeys.L_BRACKET);
		this.consumeInvalidWhitespace(TokenKeys.KW_OUT);
		this.consumeSpecificToken(TokenKeys.KW_OUT);
		this.consumeSpecificToken(TokenKeys.COMMA);
		this.consumeSpecificTokenStyle(TokenKeys.SPACE);

		const id = this.consumeString();

		this.consumeSpecificToken(TokenKeys.COMMA);
		this.consumeSpecificTokenStyle(TokenKeys.SPACE);
		this.consumeString();
		this.consumeInvalidWhitespace(TokenKeys.R_BRACKET);
		this.consumeSpecificToken(TokenKeys.R_BRACKET);

		const end = this._tokenStream.currentToken().location.end;

		const pr = new ProtectedRegion(id, Position.create(startLine, start), Position.create(startLine, end));

		const prs = this._eglAst.addProtectedRegion(pr);

		if (prs.length > 1 && this._settings.lintDuplicateProtectedRegionIds) {
			prs.forEach(x => {
				this.raisePrError(x, prs);
			});
		}
	}

	private parseOperation() {
		this.traceStep("Parse operation");
		this.consumeSpecificToken(TokenKeys.IDENTIFIER);
		this.consumeSpecificTokenStyle(TokenKeys.SPACE, "Missing space before operator");
		if (this._tokenStream.checkTokens([TokenKeys.OP_PLUS])) {
			this._tokenStream.consumeToken();
			this.consumeSpecificTokenStyle(TokenKeys.SPACE, "Missing space after operator");

			this.parseExpression();
		} else {
			this.raiseError(`Unknown token ${this._tokenStream.currentToken().name}`, DiagnosticType.SYNTAX);
		}
	}
	// -----------------------------------------------------------------------------
	//  ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗███╗   ███╗███████╗██████╗ ███████╗
	// ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║████╗ ████║██╔════╝██╔══██╗██╔════╝
	// ██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██╔████╔██║█████╗  ██████╔╝███████╗
	// ██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║╚██╔╝██║██╔══╝  ██╔══██╗╚════██║
	// ╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝██║ ╚═╝ ██║███████╗██║  ██║███████║
	//  ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝
	//
	// -----------------------------------------------------------------------------

	/**
	 * Return string contents and as it is consumed.
	 */
	private consumeString(): string {
		this.consumeSpecificTokenStyleWithAlternatives(
			TokenKeys.SINGLE_QUOTE,
			`Expected single quote, found ${this._tokenStream.currentToken().value}`,
			[TokenKeys.DOUBLE_QUOTE, TokenKeys.SINGLE_QUOTE]
		);

		const expectedTerminator: TokenKeys = this._tokenStream.previousTokenKey();
		let contents: Array<Token> = [];

		while (!this._tokenStream.checkTokens([TokenKeys.EOF, expectedTerminator])) {
			contents.push(this._tokenStream.currentToken());
			this._tokenStream.consumeToken();
		}

		this.consumeSpecificToken(expectedTerminator);

		const reducer = (accumulator: string, currentValue: Token) => accumulator + currentValue.value;
		return contents.reduce(reducer, '');
	}

	private consumeInvalidWhitespace(expectedToken: TokenKeys) {
		if (this._tokenStream.currentTokenKey() === TokenKeys.SPACE) {
			this.raiseWarning(`Expected ${expectedToken} but got SPACE`, DiagnosticType.STYLE);
			this.consumeWhitespace();
		}
	}

	private consumeWhitespace() {
		if (this._tokenStream.currentTokenKey() === TokenKeys.SPACE) {
			this.traceStep("Consume whitespace");
			this._tokenStream.consumeToken();
		}
	}

	private consumeSpecificToken(tokenKey: TokenKeys, message?: string): void {
		this.traceStep(`Consume ${tokenKey}`);
		if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
			if (message) {
				this.raiseError(message, DiagnosticType.SYNTAX);
			} else {
				this.raiseError(
					`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`,
					DiagnosticType.SYNTAX
				);
			}
		}
	}

	private consumeSpecificTokenStyleWithAlternatives(
		tokenKey: TokenKeys,
		message: string,
		validAlternatives?: Array<TokenKeys>
	): void {
		if (validAlternatives) {
			if (this._tokenStream.checkTokens(validAlternatives)) {
				this.consumeSpecificTokenStyle(tokenKey, message);
			} else {
				this.raiseError(message, DiagnosticType.SYNTAX);
			}
		} else {
			this.consumeSpecificTokenStyle(tokenKey, message);
		}
	}

	private consumeSpecificTokenStyle(tokenKey: TokenKeys, message?: string): void {
		if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
			if (message) {
				this.raiseWarning(message, DiagnosticType.STYLE);
			} else {
				this.raiseWarning(
					`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`,
					DiagnosticType.STYLE
				);
			}
		}
	}

	// ----------------------------------------------------------------------------
	// ██████╗ ██╗ █████╗  ██████╗ ███╗   ██╗ ██████╗ ███████╗████████╗██╗ ██████╗
	// ██╔══██╗██║██╔══██╗██╔════╝ ████╗  ██║██╔═══██╗██╔════╝╚══██╔══╝██║██╔════╝
	// ██║  ██║██║███████║██║  ███╗██╔██╗ ██║██║   ██║███████╗   ██║   ██║██║
	// ██║  ██║██║██╔══██║██║   ██║██║╚██╗██║██║   ██║╚════██║   ██║   ██║██║
	// ██████╔╝██║██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝███████║   ██║   ██║╚██████╗
	// ╚═════╝ ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝ ╚═════╝
	//
	// ----------------------------------------------------------------------------

	private raisePrError(pr: ProtectedRegion, prs: ProtectedRegion[]): void {
		if (!pr.Flagged) {
			const relatedDiagnosticInformation = prs
				.filter(x => x.getUniqueId() != pr.getUniqueId())
				.map(pr => {
					return DiagnosticRelatedInformation.create(
						{
							range: {
								start: pr.startPosition,
								end: pr.endPosition
							},
							uri: this._textDocument.uri
						},
						"Duplicate protected region"
					);
				});

			this.raiseDiagnosticDetail(
				"Duplicate protected region ID",
				DiagnosticSeverity.Error,
				DiagnosticType.SYNTAX,
				pr.startPosition.line,
				pr.startPosition.character,
				pr.endPosition.character,
				'',
				relatedDiagnosticInformation
			);
		}
		pr.Flagged = true;
	}

	private raiseWarning(errorMessage: string, type: DiagnosticType, code?: StaticSectionActionCode) {
		this.raiseDiagnostic(errorMessage, DiagnosticSeverity.Warning, type, code);
	}

	private raiseError(errorMessage: string, type: DiagnosticType) {
		this.raiseDiagnostic(errorMessage, DiagnosticSeverity.Error, type);
	}

	private raiseDiagnostic(message: string, severity: DiagnosticSeverity, type: DiagnosticType, code?: string): void {
		this.log(message);
		const currentToken = this._tokenStream.currentToken();
		this.raiseDiagnosticDetail(
			message,
			severity,
			type,
			currentToken.location.line,
			currentToken.location.start,
			currentToken.location.end,
			code
		);
	}

	private raiseDiagnosticDetail(
		message: string,
		severity: DiagnosticSeverity,
		type: DiagnosticType,
		line: number,
		start: number,
		end: number,
		code?: string,
		relatedInformation?: Array<DiagnosticRelatedInformation>
	) {
		this.log(`Number of errors ${this._errors.length + 1}`);

		if (this._settings.maxNumberOfProblems < this._errors.length + 1) {
			this.log(`Maximum errors reached ${this._settings.maxNumberOfProblems}, discarding the others.`);
			return;
		}

		if (!this._settings.lintStyle && type === DiagnosticType.STYLE) {
			this.log("Lint style is off, skipping");
			return;
		}
		const error: Diagnostic = {
			severity: severity,
			range: {
				start: Position.create(line, start),
				end: Position.create(line, end)
			},
			message: message,
			source: Source.epsilonLanguageEgl,
		};

		if (relatedInformation) {
			error.relatedInformation = relatedInformation;
		}

		if (code) {
			error.code = code;
		}

		this._errors.push(error);
	}

	private log(message: string) {
		if (this._settings.logLevel != LogLevel.NONE) {
			this._connection.console.log(`EglStaticSectionParser -> ${message}`);
		}
	}

	private logStep(message: string) {
		this.log(`PARSE -> ${message}`);
	}

	private traceStep(message: string) {
		this.trace(`PARSE -> ${message}`);
	}

	private trace(message: string) {
		this._connection.tracer.log(`EglStaticSectionParser -> ${message}`);
	}
}
