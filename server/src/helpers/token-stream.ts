import { Connection } from 'vscode-languageserver';
import { EOF_TOKEN, Token, TokenKeys } from '../interfaces/tokens';

export class TokenStream {
	private _tokenStream: Array<Token> = [];
	private _numberOfTokens = 0;
	private _index = 0;



	constructor(tokens: Array<Token>) {
		this._tokenStream = tokens;
		this._numberOfTokens = this._tokenStream.length;
	}

	nextToken() {
		if (this._index === this._numberOfTokens - 1) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index + 1];
	}

	nextNextToken() {
		if (this._index + 1 === this._numberOfTokens - 1) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index + 2];
	}

	nextTokenKey(): TokenKeys {
		return this.nextToken().name as TokenKeys;
	}

	nextNextTokenKey() {
		return this.nextNextToken().name as TokenKeys;
	}

	previousToken(): Token {
		if (this._index === 0) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index - 1];
	}

	previousTokenKey(): TokenKeys {
		return this.previousToken().name as TokenKeys
	}

	currentToken(): Token {
		return this._tokenStream[this._index];
	}

	consumeSpecificToken(tokenKey: TokenKeys): boolean {
		if (this.currentTokenKey() === tokenKey) {
			this.consumeToken();
			return true;
		} else {
			return false;
		}
	}

	currentTokenKey(): TokenKeys {
		return this._tokenStream[this._index].name as TokenKeys;
	}

	consumeToken(): Token {
		if (this._index === this._numberOfTokens - 1) {
			return EOF_TOKEN;
		}
		return this._tokenStream[this._index++];
	}

	/**
	 * Check if the current token matches
	 *
	 * @param token The token to check
	 */
	checkToken(token: TokenKeys, connection?: Connection): boolean {
		if (this._index === this._numberOfTokens) {
			return false;
		}
		return this._tokenStream[this._index].name as TokenKeys === token;
	}

	checkNextTwoTokens(tokens: Array<TokenKeys>): boolean {
		return tokens.includes(this.nextTokenKey())
			|| tokens.includes(this.nextNextTokenKey());
	}

	checkPreviousToken(token: TokenKeys) {
		return token === this.previousTokenKey();
	}

	checkNextTokens(tokens: Array<TokenKeys>) {
		return tokens.includes(this.nextTokenKey());
	}

	checkTokens(tokens: Array<TokenKeys>) {
		if (this._index === this._numberOfTokens - 1) {
			return false;
		}

		return tokens.includes(this.currentTokenKey());
	}

	getTokenNames() {
		return this._tokenStream.map(token => token.name);
	}
}
