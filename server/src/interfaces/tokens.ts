export enum TokenKeys {
	SPACE = "SPACE",
	L_BRACKET = "L_BRACKET",
	R_BRACKET = "R_BRACKET",
	IDENTIFIER = "IDENTIFIER",
	OP_PLUS = "OP_PLUS",
	NEW_LINE = "NEW_LINE",
	EOF = "EOF", // This is special,
	KW_PROTECTED = "KW_PROTECTED",
	KW_OUT = "KW_OUT",
	SINGLE_QUOTE = "SINGLE_QUOTE",
	DOUBLE_QUOTE = "DOUBLE_QUOTE",
	COMMA = "COMMA",
	DOT = "DOT"
}

type Matches = {[key in TokenKeys as string]: RegExp}
export const TOKEN_MATCHERS: Matches= {
	[TokenKeys.SPACE]: /\s/gm,
	[TokenKeys.L_BRACKET]: /\(/,
	[TokenKeys.R_BRACKET]: /\)/,
	[TokenKeys.SINGLE_QUOTE]: /\'/,
	[TokenKeys.DOUBLE_QUOTE]: /"/,
	[TokenKeys.DOT]: /\./,
	[TokenKeys.KW_PROTECTED]: /protected/,
	[TokenKeys.KW_OUT]: /out/,
	[TokenKeys.IDENTIFIER]: /\w+/,
	[TokenKeys.OP_PLUS]: /\+/,
	[TokenKeys.NEW_LINE]: /\n/,
	[TokenKeys.COMMA]: /,/,
};


export interface TokenLocation {
	start: number;
	end: number;
	line: number;
}

export interface Token {
	name: string;
	value: string;
	location: TokenLocation;
}

export interface Line {
	contents: string;
	number: number;
}

export const EOF_TOKEN: Token = {
	name: "EOF",
	value: "EOF",
	location: {
		start: 0,
		end: 0,
		line: 0
	}
};

