export enum Source {
	epsilonLanguage = 'Epsilon Language',
	epsilonLanguageEgl = 'Epsilon Language EGL',
	epsilonLanguageEol = 'Epsilon Language EOL',
	epsilonLanguageEgx = 'Epsilon Language EGX'
}