import { Language } from './interfaces/languages.enum';

export class UriUtils {
	static getLanguage(uri: string): Language {
		if (uri == null) {
			return Language.NONE;
		}

		
		const extension = uri.substring(uri.length - 3, uri.length);
		let language = Language.EOL;
		switch (extension) {
			case 'egl':
				language = Language.EGL;
				break;
			case 'egx':
				language = Language.EGX;
				break;
			case 'eol':
				language = Language.EOL;
				break;
		}
		return language;
	}
}